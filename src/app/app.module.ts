import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './gui/home/home/home.component';
import {LibraryModule} from "./library/library/library.module";
import {AppRoutingModule} from "./routing/app-routing.module";
import { ResetpasswordComponent } from './gui/resetpassword/resetpassword.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ResetpasswordComponent
  ],
  imports: [
    BrowserModule,
    LibraryModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
