import {Component, Input} from '@angular/core';

@Component({
  selector: 'atom-p',
  templateUrl: './p.component.html',
  styleUrls: ['./p.component.scss']
})
export class PComponent {
  /*
             Naming convention
    -----------------------------------
    @class -- string
    @id -- string
    @style -- string
    @title -- string
    @dir  -- lrt | rtl
    @lang  -- string
    @contentEditable -- boolean

   */

  @Input() class: string = '';
  @Input() id: string = '';
  @Input() style: string = '';
  @Input() title: string = '';
  @Input() dir:  'ltr' | 'rtl' = 'ltr';
  @Input() lang: string = '';
  @Input() contentEditable: boolean = false;

}
