import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PComponent} from "../p.component";



@NgModule({
  declarations: [PComponent],
  exports: [PComponent],
  imports: [
    CommonModule
  ]
})
export class PModule { }
