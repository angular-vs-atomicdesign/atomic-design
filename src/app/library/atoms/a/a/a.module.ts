import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AComponent} from "../a.component";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [AComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [AComponent]
})
export class AModule { }
