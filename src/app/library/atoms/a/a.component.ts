import {Component, Input} from '@angular/core';

@Component({
  selector: 'atom-a',
  templateUrl: './a.component.html',
  styleUrls: ['./a.component.scss']
})
export class AComponent {
  /*
           Naming convention
  -----------------------------------
  @class -- string    |
  @id -- string       |
  @link -- string     |   (router link)
  @href -- string     |   (link to redirect url)

 */

  @Input() class: string = '';
  @Input() id: string = '';
  @Input() link: string | undefined = '';
  @Input() href: string = '';

  /*
      Open the link on a new window, enable is href is defined
      -- /TODO
      -- Side effect, if you define href + link, both will be opened
   */
  openLink($any: string) {
    if(this.href !== '')
      window.open($any, '_blank');
  }
}
