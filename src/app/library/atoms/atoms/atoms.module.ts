import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AModule} from "../a/a/a.module";
import {PModule} from "../p/p/p.module";



@NgModule({
  imports: [
    CommonModule,
    AModule,
    PModule
  ],
  exports: [
    AModule,
    PModule
  ]
})
export class AtomsModule { }
