import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AtomsModule} from "../atoms/atoms/atoms.module";
import {MoleculesModule} from "../molecules/molecules/molecules.module";
import {OrganimsModule} from "../organims/organims/organims.module";



@NgModule({
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule,
    OrganimsModule
  ],
  exports: [
    AtomsModule,
    MoleculesModule,
    OrganimsModule
  ]
})
export class LibraryModule { }
