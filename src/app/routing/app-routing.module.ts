import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "../gui/home/home/home.component";
import {ResetpasswordComponent} from "../gui/resetpassword/resetpassword.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'resetPassword', component: ResetpasswordComponent },
  { path: '**', redirectTo: 'home' },
  { path: '*', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
