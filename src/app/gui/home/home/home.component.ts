import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  resetPasswordLink: string = '/resetPassword'
  baeldungLink: string = 'https://www.baeldung.com/';

  ngOnInit(): void {
  }

}
